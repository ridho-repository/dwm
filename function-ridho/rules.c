static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	        /* class                instance                title                   tags mask       switchtotag     isfloating      monitor */
/*Gimp*/	{ "Gimp",               NULL,                   NULL,                   2,              1,              0,              -1 },
/*Firefox*/	{ "Firefox",            NULL,                   NULL,                   1<<5,           1,              0,              -1 },
/*MEGAsync*/	{ NULL,                 "megasync",             "MEGAsync",             0,              0,              1,              -1 },
/*Nitrogen*/	{ "Nitrogen",           "nitrogen",             "Nitrogen",             1<<4,           1,              0,              -1 },
/*Terminator*/	{ "Terminator",         "terminator",           NULL,                   1<<2,           1,              0,              -1 },
/*st*/          { "st",                 "st",                   "st",                   1<<2,           1,              0,              -1 },
/*VLC*/         { NULL,                 "vlc",                  "VLC media player",     1<<8,           1,              0,              -1 },
/*Inkscape*/    { "Inkscape",           "inkscape",             "Inkscape",             2,              1,              0,              -1 },
/*Lxappearance*/{ NULL,                 "lxappearance",         NULL,                   1<<6,           1,              0,              -1 },
/*Thunar*/      { "Thunar",             "thunar",               NULL,                   1,              1,              0,              -1 },
/*OpenTTD*/     { NULL,                 "openttd",              NULL,                   1<<3,           1,              0,              -1 },
/*Geary*/       { NULL,                 "geary",                NULL,                   1<<7,           1,              0,              -1 },
/*surf*/        { "Surf",               "surf",                 NULL,                   1<<5,           1,              0,              -1 },
/*zathura*/     { "Zathura",            "zathura",              NULL,                   1<<6,           1,              0,              -1 },
/*tabbed-st*/   { "tabbed",             NULL,                   NULL,                   1<<2,           1,              0,              -1 },

};
