/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ Mod1Mask,                     KEY,      view,           {.ui = 1 << TAG} }, \
	{ Mod1Mask|ControlMask,         KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

static Key keys[] = {
	/* modifier                     key             function        argument */
        { Mod1Mask,                     XK_F5,          spawn,          {.v = volup } },
        { Mod1Mask,                     XK_F4,          spawn,          {.v = voldown } },
        { Mod1Mask,                     XK_k,           shiftview, { .i = +1 } },
	{ Mod1Mask,                     XK_j,           shiftview, { .i = -1 } },
        { MODKEY,                       XK_p,           spawn,          {.v = rofi } },
	{ ShiftMask,                    XK_F12,         spawn,          {.v = termcmd } },
	{ ShiftMask,                    XK_Return,      spawn,          {.v = sttb } },
        { ShiftMask,                    XK_F1,          spawn,          {.v = file } },
        { ShiftMask,                    XK_F2,          spawn,          {.v = walp } },
        { ShiftMask,                    XK_F3,          spawn,          {.v = web } },
        { ShiftMask,                    XK_F4,          spawn,          {.v = megas } },
        { ShiftMask,                    XK_F5,          spawn,          {.v = lxap } },
	{ MODKEY,                       XK_b,           togglebar,      {0} },
	{ MODKEY,                       XK_j,           focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,           focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,           incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,           incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,           setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,           setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_j,           movestack, {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,           movestack, {.i = -1 } },
	{ MODKEY,                       XK_Return,      zoom,           {0} },
	{ MODKEY,                       XK_Tab,         view,           {0} },
	{ MODKEY,                       XK_c,           killclient,     {0} },
	{ MODKEY,                       XK_t,           setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,           setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_m,           setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_space,       setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,       togglefloating, {0} },
	{ MODKEY|ShiftMask,             XK_f,           togglefullscr,  {0} },
	{ MODKEY,                       XK_0,           view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,           tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,       focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period,      focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,       tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period,      tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_minus,       setgaps,        {.i = -1 } },
	{ MODKEY,                       XK_equal,       setgaps,        {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_equal,       setgaps,        {.i = 0  } },
	TAGKEYS(                        XK_1,                           0)
	TAGKEYS(                        XK_2,                           1)
	TAGKEYS(                        XK_3,                           2)
	TAGKEYS(                        XK_4,                           3)
	TAGKEYS(                        XK_5,                           4)
	TAGKEYS(                        XK_6,                           5)
	TAGKEYS(                        XK_7,                           6)
	TAGKEYS(                        XK_8,                           7)
	TAGKEYS(                        XK_9,                           8)
	{ MODKEY|ShiftMask,             XK_q,           quit,           {0} },
        /*surf browser*/
        { MODKEY,                       XK_1,           spawn,          {.v = google } },
        { MODKEY,                       XK_2,           spawn,          {.v = suckless } },
        
        };
