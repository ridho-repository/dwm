/* appearance */
static const unsigned int borderpx  = 4;        /* border pixel of windows */
static const unsigned int gappx     = 0;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;        /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int focusonwheel       = 0;        /* focusonclick patch */
static const int user_bh            = 20;       /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const char *fonts[]          = { "Roboto Mono Medium for Powerline:size=11", "fontawesome:size=11" };
static const char dmenufont[]       = "Roboto Mono Medium for Powerline:size=11";
static const char col_ract[]        = "#f6ff42";
static const char col_tact[]        = "#00fbff";
static const char col_rback[]       = "#33324f";
static const char col_rwhite[]      = "#ffffff";
static const char col_rinactiv[]    = "#000000";
static const char col_softex[]      = "#bdd6ff";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
static const char *colors[][3]      = {
	/*                      fg              bg              border          */
	[SchemeNorm] =  {       col_softex,     col_rinactiv,   col_rinactiv    },
	[SchemeSel]  =  {       col_rinactiv,   col_tact,       col_ract        },
};

