/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[]   = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *rofi[]       = { "rofi", "-show", "run", NULL};
static const char *termcmd[]    = { "st",       NULL };
static const char *web[]        = { "firefox",          NULL };
static const char *file[]       = { "thunar",           NULL };
static const char *walp[]       = { "nitrogen",         NULL };
static const char *lxap[]       = { "lxappearance",     NULL };
static const char *megas[]      = { "megasync",         NULL };
/*volume*/
static const char *volup[]      = { "amixer", "-q", "sset", "Master", "1%+", NULL };
static const char *voldown[]    = { "amixer", "-q", "sset", "Master", "1%-", NULL };
/*surf browser*/
static const char *google[]     = { "surf",      "www.google.com",       NULL }; /*1*/
static const char *suckless[]   = { "surf",      "www.suckless.org",     NULL }; /*2*/
/*tabbed ST*/
static const char *sttb[]       = { "tabbed",   "-r",   "2",    "st",   "-w",   "''",   NULL };
