/* See LICENSE file for copyright and license details. */

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ " T ",      tile },    /* first entry is default */
	{ " M ",      monocle },
	{ " F ",      NULL },    /* no layout function means floating behavior */
};
/* tagging */
static const char *tags[] = {"","","","","","","","",""};
/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }
/*function*/
#include "function-ridho/shiftview.c"
#include "function-ridho/movestack.c"
#include "function-ridho/appearance.c"
#include "function-ridho/apps.c"
#include "function-ridho/keybind.c"
#include "function-ridho/mouse.c"
#include "function-ridho/rules.c"
